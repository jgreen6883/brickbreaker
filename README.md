A Break-Out clone with Pygame..

Installation instructions:
    Install with 'pip install brickbreaker', then 
    run 'brickbreaker' to play.

Media credits:
    
nebula background and explosion strip - Kim Lathrop
    
All of the following media came from www.opengameart.org
- paddle sprite - Aj_
- swamp sunset, desert clouds, wizard tower backgrounds - JAP
- lose ball sound - remaxim
- game over sound - Joseph Pueyo
- all level soundtracks - SketchyLogic
- drain sound (collide) - qubodup
- block explode sound - Luke.RUSTLTD


beach ball graphic from pygame.org - https://www.pygame.org/docs/tut/PygameIntro.html
